from datetime import datetime
from pymongo import MongoClient
from pprint import pprint
import json
from bson import json_util
from tqdm import tqdm
import sys

collection_name = sys.argv[1]
host = sys.argv[2]
client = MongoClient(host, username="vymo-lms-readonly", password="vymo2013readonly", authSource="vymo-lms")
start = datetime(2020, 2, 29, 18, 30, 00)
end = datetime(2020, 6, 2, 18, 29, 59)
is_leads = False

if collection_name.split("_")[-1] == "leads":
	is_leads = True

if is_leads:
	agr = [{ '$unwind': "$updates" }, {'$match': {'updates.date':{'$gte': start, '$lte': end}}}, {'$project': {"first_update_type":1, "last_update_type": 1, "code": 1, "update_type": "$updates.type", "update_name": "$updates.name", "update_date": "$updates.date", 'user_code': '$user.code', 'assigned_by': '$assigned_by.code'}}]
else:
	agr = [{ '$unwind': "$history" }, {'$match': {'history.last_updated.server_date':{'$gte': start, '$lte': end}}}, {'$project': {"event_type": "$history.event_type", "update_date": "$history.last_updated.server_date", "user_code": "$history.user.code", "code": 1, "assigned_by": "$assigned_by.code"}}]

db = client['vymo-lms']
collection = db[collection_name]
val = collection.aggregate(agr)
with open('{}.json'.format(collection_name), 'w') as f:
	for entry in val:
		json_entry = json.dumps(entry, default=json_util.default)
		f.write(json_entry + "\n")
